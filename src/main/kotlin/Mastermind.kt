import java.io.File
import java.util.*
val scanner = Scanner(System.`in`)

val file777= File("src/main/resources/777Historial.txt")
val fileClasic=File("src/main/resources/clasicHistorial.txt")
//Declaració de colors

val color=" "
val R="\u001B[41m $color"
val G= "\u001B[42m $color"
val B= "\u001B[44m $color"
val Y="\u001B[103m $color"
val P="\u001B[45m $color"
val O="\u001B[43m $color"
val T="\u001B[106m $color"

val correct="\u001B[40m $color"
val semi= "\u001B[107m $color"
val mal= "\u001B[47m $color"
//Fin declaració de colors
var mode777=true

/**
 * @author Joel Garcia
 *
 * @param colors Rep la llista de 5 colors
 * @return Torna una llista de 4 colors amb ordre aleatori
 */

fun createSol(colors:List<String>):MutableList<String>{
    val solution= mutableListOf<String>()

    val sol1=(colors.random())
    solution.add(0, sol1)

    val sol2=(colors.random())
    solution.add(1, sol2)

    val sol3=colors.random()
    solution.add(2, sol3)

    val sol4=colors.random()
    solution.add(3, sol4)
    return solution
}
/**
 * @author Joel Garcia
 *
 * @param colors777 Rep la llista de 7 colors
 * @return Torna una llista de colors amb ordre aleatori
 */
fun createSol777(colors777:List<String>):MutableList<String>{
    val solution= mutableListOf<String>()

    val sol1=(colors777.random())
    solution.add(sol1)

    val sol2=(colors777.random())
    solution.add(sol2)

    val sol3=colors777.random()
    solution.add(sol3)

    val sol4=colors777.random()
    solution.add(sol4)

    val sol5=colors777.random()
    solution.add(sol5)

    val sol6=colors777.random()
    solution.add(sol6)

    val sol7=colors777.random()
    solution.add(sol7)

    return solution
}
/**
 * @author Joel Garcia
 *
 * @param respuesta Rep la resposta de l'usuari
 * @param R Cuadre vermell
 * @param G Cuadre verd
 * @param B Cuadre blau
 * @param Y Cuadre groc
 * @param P Cuadre lila
 * @param T Cuadre turquesa
 * @param O Cuadre or
 *
 * @return Torna el colos corresponent a la primera posició de la resposta
 */
fun resp1 (respuesta:List<String>,R:String,G:String,B:String,P:String,Y:String,O:String,T:String): String {
    var color1= String()
    if (respuesta[1]=="R") color1=R
    else if (respuesta[1]=="G") color1=G
    else if (respuesta[1]=="B") color1=B
    else if (respuesta[1]=="P") color1=P
    else if (respuesta[1]=="O") color1=O
    else if (respuesta[1]=="T") color1=T
    else color1=Y
    return color1
}
/**
 * @author Joel Garcia
 *
 * @param respuesta Rep la resposta de l'usuari
 * @param R Cuadre vermell
 * @param G Cuadre verd
 * @param B Cuadre blau
 * @param Y Cuadre groc
 * @param P Cuadre lila
 * @param T Cuadre turquesa
 * @param O Cuadre or
 *
 * @return Torna el colos corresponent a la segona posició de la resposta
 */
fun resp2 (respuesta:List<String>,R:String,G:String,B:String,P:String,Y:String,O:String,T:String): String {
    var color2=String()
    if (respuesta[2]=="R") color2=R
    else if (respuesta[2]=="G") color2=G
    else if (respuesta[2]=="B") color2=B
    else if (respuesta[2]=="P") color2=P
    else if (respuesta[2]=="O") color2=O
    else if (respuesta[2]=="T") color2=T
    else color2=Y
    return color2
}
/**
 * @author Joel Garcia
 *
 * @param respuesta Rep la resposta de l'usuari
 * @param R Cuadre vermell
 * @param G Cuadre verd
 * @param B Cuadre blau
 * @param Y Cuadre groc
 * @param P Cuadre lila
 * @param T Cuadre turquesa
 * @param O Cuadre or
 *
 * @return Torna el colos corresponent a la tercera posició de la resposta
 */
fun resp3 (respuesta:List<String>,R:String,G:String,B:String,P:String,Y:String,O:String,T:String): String {
    var color3=String()
    if (respuesta[3]=="R") color3=R
    else if (respuesta[3]=="G") color3=G
    else if (respuesta[3]=="B") color3=B
    else if (respuesta[3]=="P") color3=P
    else if (respuesta[3]=="O") color3=O
    else if (respuesta[3]=="T") color3=T
    else color3=Y
    return color3
}
/**
 * @author Joel Garcia
 *
 * @param respuesta Rep la resposta de l'usuari
 * @param R Cuadre vermell
 * @param G Cuadre verd
 * @param B Cuadre blau
 * @param Y Cuadre groc
 * @param P Cuadre lila
 * @param T Cuadre turquesa
 * @param O Cuadre or
 *
 * @return Torna el colos corresponent a la cuarta posició de la resposta
 */
fun resp4 (respuesta:List<String>,R:String,G:String,B:String,P:String,Y:String,O:String,T:String): String {
    var color4=String()
    if (respuesta[4]=="R") color4=R
    else if (respuesta[4]=="G") color4=G
    else if (respuesta[4]=="B") color4=B
    else if (respuesta[4]=="P") color4=P
    else if (respuesta[4]=="O") color4=O
    else if (respuesta[4]=="T") color4=T
    else color4=Y
    return color4
}
/**
 * @author Joel Garcia
 *
 * @param respuesta Rep la resposta de l'usuari
 * @param R Cuadre vermell
 * @param G Cuadre verd
 * @param B Cuadre blau
 * @param Y Cuadre groc
 * @param P Cuadre lila
 * @param T Cuadre turquesa
 * @param O Cuadre or
 *
 * @return Torna el colos corresponent a la cinquena posició de la resposta
 */
fun resp5 (respuesta:List<String>,R:String,G:String,B:String,P:String,Y:String,O:String,T:String): String {
    var color5=String()
    if (respuesta[5]=="R") color5=R
    else if (respuesta[5]=="G") color5=G
    else if (respuesta[5]=="B") color5=B
    else if (respuesta[5]=="P") color5=P
    else if (respuesta[5]=="O") color5=O
    else if (respuesta[5]=="T") color5=T
    else color5=Y
    return color5
}
/**
 * @author Joel Garcia
 *
 * @param respuesta Rep la resposta de l'usuari
 * @param R Cuadre vermell
 * @param G Cuadre verd
 * @param B Cuadre blau
 * @param Y Cuadre groc
 * @param P Cuadre lila
 * @param T Cuadre turquesa
 * @param O Cuadre or
 *
 * @return Torna el colos corresponent a la sisena posició de la resposta
 */
fun resp6 (respuesta:List<String>,R:String,G:String,B:String,P:String,Y:String,O:String,T:String): String {
    var color6=String()
    if (respuesta[6]=="R") color6=R
    else if (respuesta[6]=="G") color6=G
    else if (respuesta[6]=="B") color6=B
    else if (respuesta[6]=="P") color6=P
    else if (respuesta[6]=="O") color6=O
    else if (respuesta[6]=="T") color6=T
    else color6=Y
    return color6
}
/**
 * @author Joel Garcia
 *
 * @param respuesta Rep la resposta de l'usuari
 * @param R Cuadre vermell
 * @param G Cuadre verd
 * @param B Cuadre blau
 * @param Y Cuadre groc
 * @param P Cuadre lila
 * @param T Cuadre turquesa
 * @param O Cuadre or
 *
 * @return Torna el colos corresponent a la setena posició de la resposta
 */
fun resp7 (respuesta:List<String>,R:String,G:String,B:String,P:String,Y:String,O:String,T:String): String {
    var color7=String()
    if (respuesta[7]=="R") color7=R
    else if (respuesta[7]=="G") color7=G
    else if (respuesta[7]=="B") color7=B
    else if (respuesta[7]=="P") color7=P
    else if (respuesta[7]=="O") color7=O
    else if (respuesta[7]=="T") color7=T
    else color7=Y
    return color7
}
/**
 * @author Joel Garcia
 *
 * @param solution Rep la solució aleatoria generada anteriorment
 * @param resp Rep una llista amb la rasposta de l'usuari
 * @param correct Cuadre negre
 * @param semi Cuadre blanc
 * @param mal Cuadre gris
 *
 * @return Torna una llista amb la correcció de cada posició
 */
fun repeticiones(solution: MutableList<String>, resp: MutableList<String>,correct:String,semi:String,mal:String):MutableList<String>{
    var correctorList= MutableList(4){""}
    if(mode777==true)correctorList= MutableList(7){""}

    val checkerList= mutableListOf<String>()
    for(i in 0..resp.lastIndex){
        if (resp[i]==solution[i]){
            correctorList[i]=correct
        }
        else{checkerList.add(solution[i])
        }

    }
    for (i in 0..resp.lastIndex){
        if (resp[i] in checkerList && correctorList[i]==""){
            correctorList[i]=semi
        }
        else if(resp[i] !in checkerList && correctorList[i]==""){
            correctorList[i]=mal
        }
    }
    return correctorList
    }
/**
 * @author Joel Garcia
 *
 * Execuata el mode de joc clasic
 *
 * @param name Rep el nom de l'usuari
 */
fun clasicMode(name:String){
    println("Com marcar un color: R per a ${R+"\u001B[0m $color"}/ G per a ${G+"\u001B[0m $color"}/ B per a ${B+"\u001B[0m $color"}/ Y per a ${Y+"\u001B[0m $color"}/ P per a ${P+"\u001B[0m $color"}")
    println("Comprobació de la resposta: Si el color introduit està en la posició correcte s'indicarà amb ${correct+"\u001B[0m $color"}, ")
    println("si el color hi ès a la resposta però en altre posició s'indicarà amb ${semi+"\u001B[0m $color"}I si no hi es a la resposta, amb ${mal+"\u001B[0m $color"}")
        val resp = mutableListOf<String>()
        val colors = listOf(R, G, B, Y, P)
        val solution = createSol(colors)
        println(" ")
        val historial = mutableListOf("", "", "", "", "", "")

        for (i in 0 until 6) {
            println("INTRODEIX LA TEVA RESPOSTA")
            val answer = scanner.next().uppercase()
            val respuesta = answer.split("")

            val color1 = resp1(respuesta, R, G, B, P, Y, O, T)
            resp.add(color1)
            val color2 = resp2(respuesta, R, G, B, P, Y, O, T)
            resp.add(color2)
            val color3 = resp3(respuesta, R, G, B, P, Y, O, T)
            resp.add(color3)
            val color4 = resp4(respuesta, R, G, B, P, Y, O, T)
            resp.add(color4)

            println("\u001B[0m $color")
            val corrector1 = repeticiones(solution, resp, correct, semi, mal)[0]
            val corrector2 = repeticiones(solution, resp, correct, semi, mal)[1]
            val corrector3 = repeticiones(solution, resp, correct, semi, mal)[2]
            val corrector4 = repeticiones(solution, resp, correct, semi, mal)[3]

            //Afegim la resposta y la correcció a la llista y la imprimim
            val acumilador =
                (color1 + color2 + color3 + color4 + "\u001B[0m $color" + corrector1 + corrector2 + corrector3 + corrector4 + "\u001B[0m $color")
            historial.set(i, acumilador)

            println(historial[0])
            println(historial[1])
            println(historial[2])
            println(historial[3])
            println(historial[4])
            println(historial[5])
            //Fi impresió

            if (resp == solution) {
                println("RESPOSTA CORRECTE")
                break
            }
            resp.clear()
        }
        if (resp != solution) {
            println("GAME OVER")
            println(solution[0] + solution[1] + solution[2] + solution[3] + "\u001B[0m $color")
        }
    fileClasic.appendText("$name\n")
    fileClasic.appendText("${historial[0]}\n")
    fileClasic.appendText("${historial[1]}\n")
    fileClasic.appendText("${historial[2]}\n")
    fileClasic.appendText("${historial[3]}\n")
    fileClasic.appendText("${historial[4]}\n")
    fileClasic.appendText("${historial[5]}\n")


        }
/**
 * @author Joel Garcia
 *
 * Execuata el mode de joc 777
 *
 * @param name Rep el nom de l'usuari
 */
fun mode777(name:String){
    println("Com marcar un color: R per a ${R+"\u001B[0m $color"}/ G per a ${G+"\u001B[0m $color"}/ B per a ${B+"\u001B[0m $color"}/ Y per a ${Y+"\u001B[0m $color"}")
    println("/ P per a ${P+"\u001B[0m $color"}/ T per a ${T+"\u001B[0m $color"}/ O per a ${O+"\u001B[0m $color"}")
    println("Comprobació de la resposta: Si el color introduit està en la posició correcte s'indicarà amb ${correct+"\u001B[0m $color"}, ")
    println("si el color hi ès a la resposta però en altre posició s'indicarà amb ${semi+"\u001B[0m $color"}I si no hi es a la resposta, amb ${mal+"\u001B[0m $color"}")
        val resp = mutableListOf<String>()
        val colors777 = listOf(R, G, B, Y, P, O, T)
        val solution = createSol777(colors777)
        println(" ")
        val historial = mutableListOf("", "", "", "", "", "", "")

        for (i in 0 until 7) {
            println("INTRODEIX LA TEVA RESPOSTA")
            val answer = scanner.next().uppercase()
            val respuesta = answer.split("")

            val color1 = resp1(respuesta, R, G, B, P, Y, O, T)
            resp.add(color1)
            val color2 = resp2(respuesta, R, G, B, P, Y, O, T)
            resp.add(color2)
            val color3 = resp3(respuesta, R, G, B, P, Y, O, T)
            resp.add(color3)
            val color4 = resp4(respuesta, R, G, B, P, Y, O, T)
            resp.add(color4)
            val color5 = resp5(respuesta, R, G, B, P, Y, O, T)
            resp.add(color5)
            val color6 = resp6(respuesta, R, G, B, P, Y, O, T)
            resp.add(color6)
            val color7 = resp7(respuesta, R, G, B, P, Y, O, T)
            resp.add(color7)

            println("\u001B[0m $color")
            val corrector1 = repeticiones(solution, resp, correct, semi, mal)[0]
            val corrector2 = repeticiones(solution, resp, correct, semi, mal)[1]
            val corrector3 = repeticiones(solution, resp, correct, semi, mal)[2]
            val corrector4 = repeticiones(solution, resp, correct, semi, mal)[3]
            val corrector5 = repeticiones(solution, resp, correct, semi, mal)[4]
            val corrector6 = repeticiones(solution, resp, correct, semi, mal)[5]
            val corrector7 = repeticiones(solution, resp, correct, semi, mal)[6]

            //Afegim la resposta y la correcció a la llista y la imprimim
            val acumilador =
                (color1 + color2 + color3 + color4 + color5 + color6 + color7 + "\u001B[0m $color" + corrector1 + corrector2 + corrector3 + corrector4 + corrector5 + corrector6+ corrector7+ "\u001B[0m $color")
            historial.set(i, acumilador)

            println(historial[0])
            println(historial[1])
            println(historial[2])
            println(historial[3])
            println(historial[4])
            println(historial[5])
            println(historial[6])
            //Fi impresió

            if (resp == solution) {
                println("RESPOSTA CORRECTE")
                break
            }
            resp.clear()
        }
        if (resp != solution) {
            println("GAME OVER")
            println(solution[0] + solution[1] + solution[2] + solution[3] + solution[4] + solution[5] + solution[6] + "\u001B[0m $color")
        }
    file777.appendText("$name\n")
    file777.appendText("${historial[0]}\n")
    file777.appendText("${historial[1]}\n")
    file777.appendText("${historial[2]}\n")
    file777.appendText("${historial[3]}\n")
    file777.appendText("${historial[4]}\n")
    file777.appendText("${historial[5]}\n")

    }

fun main(){



    println("INTRODUEIX EL TEU NOM:")
    val name= scanner.next()
    println("Benvingut a MasterMind, en aquest joc hauràs de trobar la seqüència de colors correcte")
    println("A tenir en compte: La resposta correcte es una sucesió colors amb ordre aleatori")
    println("Com jugar: Has d'introduir la seqüencia de lletres corresponent a cada color i polsar ENTER")

    var retry=true
    while (retry){
        val action=scanner.nextLine()
        println("historial 777\n" +
                "historial clasic\n" +
                "jugar\n" +
                "cerrar")
        when(action.lowercase()){
            "historial 777"->println(file777.readText())
            "historial clasic"->println(fileClasic.readText())
            "cerrar"->kotlin.system.exitProcess(0)
            "jugar"->{
                println("TRIA UN MODE: 7 PER AL MODE 777/ C PER AL MODE CLASSIC")
                var modeInput=scanner.next()
                while (true){
                    if(modeInput.uppercase()=="C"){
                        mode777=false
                        break
                    }
                    else if(modeInput.uppercase()=="7"){
                        mode777=true
                        break
                    }
                    else {
                        println("\u001B[91m Aquest mode no existeix, torna a provar\u001B[0m ")
                        modeInput=scanner.next()
                    }
                }
                if(mode777==false)clasicMode(name)
                else mode777(name)

                println("VOLS TORNAR A JUGAR? S/N")
                var repeat = scanner.next()
                while (true){
                    if (repeat.uppercase() == "N"){
                        retry = false
                        break
                    }
                    else if(repeat.uppercase() == "S") {
                        retry = true
                        break
                    }
                    else if (repeat.uppercase() != "N" && repeat.uppercase() != "S"){
                        println("\u001B[91m Aquesta opció no es valida, torna a provar\u001B[0m ")
                        repeat = scanner.next()
                    }
                }
            }
        }

    }


}