# MasterMind
## Descripció del Projecte

Es tracta de una solucióque permet jugar MasterMind des de la terminal d'InteliJ.<br>
El projecte està realitzat amb llenguatge Kotlin.

## Com instal·lar i executar el projecte

Per a instal·lar MasterMind cal introduir el codi a un projecte de Kotlin. <br>
Una vegada instalat, només falta polsar el botó RUN per a executar y jugar a la terminal.

## Com utilitzar el projecte



Es pot triar entre dos modes de joc:

### Mode clasic:

Hi ha una seqüencia de 4 colors amb ordere aleatori(hi han 5 possibles colors).<br>
Per a introduir una resposta escriu les lletres corresponents a la terminal (Ex: rgbp)<br>
Hi han 6 intents per a trobar la resposta, i a cada torn es mostrarà la correcció de la resposta.

### Mode 777:

Hi ha una seqüencia de 7 colors amb ordere aleatori(hi han 7 possibles colors).<br>
Per a introduir una resposta escriu les lletres corresponents a la terminal (Ex: rgbprto)<br>
Hi han 7 intents per a trobar la resposta, i a cada torn es mostrarà la correcció de la resposta.

## 
També es pot consultar l'historial de partides anteriors de cada mode de joc

